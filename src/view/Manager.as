package view 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class Manager extends Sprite 
	{
		public const FRAME:uint = 120;
		protected const GRAVITY:Number = 0.1;
		protected var targetList:Array;
		protected var ballList:Array;
		public function Manager() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		protected function layout():void 
		{
			trace("hoge")
		}
		
	}

}