package view 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class Ball extends Sprite 
	{
		public var target:uint = 0;
		public var frame:uint = 0;
		public var vx:Number = 0;
		public var vy:Number = 0;
		public function Ball() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{
			var g:Graphics = this.graphics;
			g.beginFill(0x0);
			g.drawCircle(0, 0, 3);
		}
		
	}

}