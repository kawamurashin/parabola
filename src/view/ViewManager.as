package view 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author jaiko
	 */
	public class ViewManager extends Manager 
	{
		
		public function ViewManager() 
		{
			super();
			
		}
		override protected function layout():void 
		{
			super.layout();
			var i:uint;
			var n:uint;
			var g:Graphics;
			var target:Sprite;
			//
			ballList = [];
			targetList = [];
			n = 6;
			for (i = 0; i < n; i++)
			{
				target = new Sprite();
				addChild(target);
				g = target.graphics;
				g.beginFill(0xFF0000);
				g.drawCircle(0, 0, 5);
				target.x = 20 + (stage.stageWidth - 40 ) * Math.random();
				target.y = 20 + (stage.stageHeight - 40) * Math.random();
				targetList.push(target);
				
			}
			
			stage.addEventListener(MouseEvent.CLICK, clickHandler);
			
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		private function enterFrameHandler(e:Event):void 
		{
			var i:uint;
			var n:uint;
			var ball:Ball;
			 n = ballList.length;
			 for (i = 0; i < n; i++)
			 {
				 ball = ballList[i];
				 ball.vy += GRAVITY;
				 ball.x += ball.vx;
				 ball.y += ball.vy;
				 ball.frame++;
			 }
			 checkBallPosition();
		}
		
		private function checkBallPosition():void 
		{
			var i:uint;
			var n:uint;
			var ball:Ball;
			var target:Sprite;
			//
			n = ballList.length;
			for (i = 0; i < n; i++)
			{
				ball = ballList[i];
				if (ball.frame == FRAME)
				{
					
					ball.frame = 0;
					ball.target++;
					if (ball.target == targetList.length)
					{
						ball.target = 0;
					}
					setVector(ball);
				}
			}
			
		}
		
		private function clickHandler(e:MouseEvent):void 
		{


			
			var ball:Ball = new Ball();
			addChild(ball);
			ball.x = targetList[0].x;
			ball.y = targetList[0].y;
			ball.target = 0;
			setVector(ball);
			//
			ballList.push(ball);

		}
		private function setVector(ball:Ball):void
		{
			var dx:Number;
			var dy:Number;
			
			var currentTarget:Sprite;
			var nextTarget:Sprite;
			//
			currentTarget = targetList[ball.target];
			if (ball.target +1 == targetList.length)
			{
				nextTarget = targetList[0];
			}
			else
			{
				nextTarget = targetList[ball.target + 1];
			}
			
			dx = nextTarget.x - currentTarget.x;
			ball.vx = dx / FRAME;
			//
			dy = nextTarget.y - currentTarget.y;
			// 1/2 * t*t*g
			ball.vy = (dy - (0.5 * FRAME * FRAME * GRAVITY)) / FRAME;
			
			ball.x = currentTarget.x;
			ball.y = currentTarget.y;
			
		}
		
	}

}