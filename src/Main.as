package 
{
	import adobe.utils.CustomActions;
	import flash.display.Sprite;
	import flash.events.Event;
	import view.Manager;
	import view.ViewManager;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			var viewManager:ViewManager = new ViewManager();
			addChild(viewManager);
		}
		
	}
	
}